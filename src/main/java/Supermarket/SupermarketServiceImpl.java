package Supermarket;

import CashRegister.CashRegister;
import Colors.Colors;
import Exceptions.BuyerChangedMindException;
import Exceptions.NotEnoughChangeException;
import Exceptions.PayNotAcceptedException;
import Exceptions.SoldOutException;
import Product.Product;
import Product.ProductStorage;

import java.util.*;
import java.util.stream.Collectors;

public class SupermarketServiceImpl implements SupermarketService {
    private static SupermarketServiceImpl supermarketServiceImpl;
    public Scanner scanner = new Scanner(System.in);
    private Product selectedProduct = null;
    private int toPay;
    Map<Double, Integer> returnChange = new TreeMap<>();
    Map<Double, Integer> paidCash = new TreeMap<>();

    private SupermarketServiceImpl() {}

    public static SupermarketServiceImpl getSupermarketServiceImpl() {
        if (supermarketServiceImpl == null) {
            supermarketServiceImpl = new SupermarketServiceImpl();
        }
        return supermarketServiceImpl;
    }

    public void runSupermarket(List<Product> products, ProductStorage productStorage, List<CashRegister> cashRegister) {

        System.out.println(Colors.ANSI_BRIGHT_PURPLE + "Welcome to the store. You can quit at any time by typing x." + Colors.ANSI_RESET);
        while (true) {
            try {
                supermarketServiceImpl.getSelection(products, productStorage);
                supermarketServiceImpl.getChange(cashRegister);
                supermarketServiceImpl.getReceipt();
                supermarketServiceImpl.updateStorageAndCashRegister(productStorage, cashRegister);
                if(toCancel()){break;}
            } catch (BuyerChangedMindException e) {
                System.out.println(Colors.ANSI_BRIGHT_PURPLE + "Thanks for stopping by." + Colors.ANSI_RESET);
                break;
            } catch (SoldOutException e) {
                System.out.println(Colors.ANSI_BRIGHT_RED + "Requested product does not exist or has been sold out." + Colors.ANSI_RESET);
                if(toCancel()){break;}
            } catch (NotEnoughChangeException e) {
                System.out.println(Colors.ANSI_BRIGHT_RED + "Shop does not have sufficient change to complete transaction." + Colors.ANSI_RESET);
                if(toCancel()){break;}
            }

            selectedProduct = null;
            returnChange.clear();
            paidCash.clear();
            toPay = 0;
        }

    }

    @Override
    public void getSelection(List<Product> products, ProductStorage productStorage) throws BuyerChangedMindException, SoldOutException {
        System.out.println(Colors.ANSI_BRIGHT_BLUE
                + "What would you like to buy? Type in the product number. Store selection: " + Colors.ANSI_RESET);
        products.forEach((product) -> System.out.println(Colors.ANSI_BRIGHT_WHITE
                + product.getIndex() + ". " + product.getName()
                + ", price: " + product.getPrice() + Colors.ANSI_RESET));

        int selectedNumber = checkInput("selection");
        if (selectedNumber == -1) {
            throw new BuyerChangedMindException();
        }
        selectedProduct = products.stream()
                .filter(product -> selectedNumber == product.getIndex()).findFirst().orElse(null);

        if (selectedProduct == null) {
            throw new SoldOutException();
        } else if (productStorage.getProductFromStorage(selectedProduct.getName()) == 0) {
            throw new SoldOutException();
        } else {
            System.out.println(Colors.ANSI_BRIGHT_BLUE + "For " + selectedProduct.getName()
                    + " you have to pay " + selectedProduct.getPrice() + "." + Colors.ANSI_RESET);
        }

    }

    @Override
    public void getChange(List<CashRegister> cashRegister) throws BuyerChangedMindException, NotEnoughChangeException {
        int productPrice = (int) (selectedProduct.getPrice() * 100);
        int paidValue;
        boolean paidValueInRegister;
        int paidTotal = 0;

        System.out.println(Colors.ANSI_BRIGHT_BLUE + "Provide bill or coin. Store accepts "
                + cashToPrint(cashRegister) + "." + Colors.ANSI_RESET);

        do {
            paidValue = checkInput("cash");
            if (paidValue == -1) {
                throw new BuyerChangedMindException();
            }

            int finalPaidValue = paidValue;
            paidValueInRegister = cashRegister.stream()
                    .map(cash -> (int) (cash.getValue() * 100))
                    .anyMatch(value -> value == finalPaidValue);

            try {
                checkPayNotAccepted(paidValueInRegister);
            } catch (PayNotAcceptedException e) {
                System.out.println(Colors.ANSI_BRIGHT_RED + "Bill or coin is not supported." + Colors.ANSI_RESET);
                System.out.println(Colors.ANSI_BRIGHT_BLUE
                        + "Provide bill or coin from the list: " + cashToPrint(cashRegister) + " or type x to cancel." + Colors.ANSI_RESET);
                continue;
            }

            paidTotal += paidValue;
            updatePaidCash(paidValue);
            toPay = productPrice - paidTotal;

            if (productPrice > paidTotal) {
                System.out.println(Colors.ANSI_BRIGHT_BLUE + "You paid " + String.format("%.2f", (float) paidTotal / 100) + " in total and need to pay "
                        + String.format("%.2f", (float) toPay / 100) + " more. Provide bill or coin." + Colors.ANSI_RESET);
            } else {
                calculateChange(cashRegister);
            }
        } while (productPrice > paidTotal);

    }

    @Override
    public void getReceipt() {
        String printChange = returnChange.keySet().stream()
                .map(key -> key + " count: " + returnChange.get(key))
                .collect(Collectors.joining(", value: ", "(value: ", ")."));

        System.out.println(Colors.ANSI_BRIGHT_PURPLE + "Here is your " + selectedProduct.getName() + "." + Colors.ANSI_RESET);
        if (toPay != 0) {
            System.out.println(Colors.ANSI_BRIGHT_PURPLE + "Here is your change: "
                    + String.format("%.2f", (float) Math.abs(toPay) / 100) + " " + printChange + Colors.ANSI_RESET);
        }
        System.out.println(Colors.ANSI_BRIGHT_PURPLE + "Thanks for buying." + Colors.ANSI_RESET);
    }

    @Override
    public void updateStorageAndCashRegister(ProductStorage productStorage, List<CashRegister> cashRegister) {
        productStorage.updateQuantity(selectedProduct.getName());

        returnChange.forEach((value, count) -> cashRegister.stream()
                .filter(cash -> Objects.equals(value, cash.getValue())).findFirst()
                .ifPresent(fromCash -> fromCash.setQuantity(fromCash.getQuantity() - count)));

        paidCash.forEach((value, count) -> cashRegister.stream()
                .filter(cash -> Objects.equals(value, cash.getValue())).findFirst()
                .ifPresent(toCash -> toCash.setQuantity(toCash.getQuantity() + count)));

    }

    private void updatePaidCash(int paidValue) {
        double paidDouble = paidValue / 100.0;
        if (paidCash.containsKey(paidDouble)) {
            paidCash.put(paidDouble, paidCash.get(paidDouble)+1);
        } else {
            paidCash.put(paidDouble, 1);
        }
    }

    private void calculateChange(List<CashRegister> cashRegister) throws NotEnoughChangeException {
        int change = Math.abs(toPay);
        int value;
        double valueDouble;
        int valueInt;
        int count;

        for (int i = (cashRegister.size()-1); i >= 0; i--) {
            valueDouble = cashRegister.get(i).getValue();
            valueInt = (int) (cashRegister.get(i).getValue() * 100);

            count = change/valueInt;
            change %= valueInt;
            if (count != 0) {
                value = checkIfEnoughInRegister(cashRegister, valueDouble, count);
                change += value;
            }
        }

        if (change != 0) {
            throw new NotEnoughChangeException();
        }

    }

    private int checkIfEnoughInRegister(List<CashRegister> cashRegister, double value, int count) {
        CashRegister cashInstance = cashRegister.stream()
                .filter(cash -> Objects.equals(value, cash.getValue())).findFirst().orElse(null);

        if (cashInstance != null) {
            if (cashInstance.getQuantity() >= count) {
                returnChange.put(cashInstance.getValue(), count);
                return 0;
            } else if (count == 1) {
                return (int) (value * 100);
            } else {
                for (int i = count-1; i > 0; i--) {
                    if (cashInstance.getQuantity() >= i) {
                        returnChange.put(cashInstance.getValue(), count);
                        return (int) (value * 100) * (count-i);
                    }
                }
                return (int) (value * 100) * count;
             }
        } else {
            return (int) (value * 100) * count;
        }

    }

    private int checkInput(String inputPart) {
        int validInput;

        if (scanner.hasNextInt()) {
            validInput = scanner.nextInt();
            if (validInput > 0) {
                if (Objects.equals(inputPart, "cash")) {
                    return validInput * 100;
                } else {return validInput;}
            } else {
                System.out.println(Colors.ANSI_BRIGHT_BLUE + "Enter a valid number or type x to cancel." + Colors.ANSI_RESET);
                return checkInput(inputPart);
            }
        } else if (scanner.hasNextDouble()) {
            if (Objects.equals(inputPart, "selection")) {
                System.out.println(Colors.ANSI_BRIGHT_BLUE + "Enter a valid number or type x to cancel." + Colors.ANSI_RESET);
                scanner.next();
                return checkInput(inputPart);
            } else {
                validInput = (int) (scanner.nextDouble() * 100);
                return validInput;
            }
        } else {
            if (Objects.equals(scanner.next().toLowerCase(), "x")) {
                return -1;
            } else {
                System.out.println(Colors.ANSI_BRIGHT_BLUE + "Enter a valid number or type x to cancel." + Colors.ANSI_RESET);
                return checkInput(inputPart);
            }
        }

    }

    private String cashToPrint(List<CashRegister> cashRegister) {
        return cashRegister.stream()
                .map(CashRegister::getValue).sorted()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }

    private void checkPayNotAccepted(boolean paidValueInRegister) throws PayNotAcceptedException {
        if (!paidValueInRegister) {
            throw new PayNotAcceptedException();
        }
    }

    private boolean toCancel() {
        System.out.println(Colors.ANSI_BRIGHT_BLUE + "Do you want to check out store again? Press anything to continue, x to cancel." + Colors.ANSI_RESET);
        if (Objects.equals(scanner.next().toLowerCase(), "x")) {
            System.out.println(Colors.ANSI_BRIGHT_PURPLE + "Thanks for stopping by." + Colors.ANSI_RESET);
            return true;
        }
        return false;
    }

}
