package Supermarket;

import Exceptions.BuyerChangedMindException;
import Exceptions.NotEnoughChangeException;
import Exceptions.SoldOutException;
import Product.Product;
import Product.ProductStorage;
import CashRegister.CashRegister;

import java.util.List;

public interface SupermarketService {
    void runSupermarket(List<Product> products, ProductStorage productStorage, List<CashRegister> cashRegister);
    void getSelection(List<Product> products, ProductStorage productStorage) throws BuyerChangedMindException, SoldOutException;
    void getChange(List<CashRegister> cashRegister) throws BuyerChangedMindException, NotEnoughChangeException;
    void getReceipt();
    void updateStorageAndCashRegister(ProductStorage productStorage, List<CashRegister> cashRegister);
}
