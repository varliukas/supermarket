import CashRegister.CashRegister;
import Colors.Colors;
import Product.Product;
import Product.ProductStorage;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class ReadCsvFiles {

    static void readProducts(String initialProductsFile, List<Product> products, ProductStorage productStorage) {
        try (CSVReader csvReader = new CSVReaderBuilder(new FileReader(initialProductsFile)).withSkipLines(1).build()) {
            String[] line;
            Product product;
            int index = 1;

            while ((line = csvReader.readNext()) != null) {
                if (line.length == 3 && !line[0].isEmpty() && !line[1].trim().isEmpty()) {
                    product = new Product(index, line[0], Double.parseDouble(line[1].trim()));
                    products.add(product);
                    if (!line[2].trim().isEmpty()) {
                        productStorage.addProduct(line[0], Integer.parseInt(line[2].trim()));
                    } else {
                        productStorage.addProduct(line[0], 0);
                    }
                } else {
                    if (line.length >= 2 && !line[0].trim().isEmpty() && line[1].trim().isEmpty()) {
                        System.out.println(Colors.ANSI_BRIGHT_YELLOW + "Price for " + line[0] + " is incorrect and record was not added" + Colors.ANSI_RESET);
                    } else {
                        System.out.println(Colors.ANSI_BRIGHT_YELLOW + "At least one record in Products file is incorrect and was not added" + Colors.ANSI_RESET);
                    }
                }
                index++;
            }
        } catch (IOException | CsvValidationException e) {
            System.out.println(e);
        }
    }

    static void readCashRegister(String initialCashRegisterFile, List<CashRegister> cashRegister) {
        try (CSVReader csvReader = new CSVReaderBuilder(new FileReader(initialCashRegisterFile)).withSkipLines(1).build()) {
            String[] line;
            CashRegister cash;

            while ((line = csvReader.readNext()) != null) {
                if (line.length == 2 && !line[0].trim().isEmpty()) {
                    if (!line[1].isEmpty()) {
                        cash = new CashRegister(Double.parseDouble(line[0].trim()), Integer.parseInt(line[1].trim()));
                    } else {
                        cash = new CashRegister(Double.parseDouble(line[0].trim()), 0);
                    }
                    cashRegister.add(cash);
                } else {
                    System.out.println(Colors.ANSI_BRIGHT_YELLOW + "At least one record in CashRegister file is incorrect and was not added" + Colors.ANSI_RESET);
                }
            }
        } catch (IOException | CsvValidationException e) {
            System.out.println(e);
        }
    }

}
