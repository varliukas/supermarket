package CashRegister;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CashRegister {
    private Double value;
    private Integer quantity;
}
