package Product;

import Colors.Colors;

import java.util.Map;
import java.util.TreeMap;

public class ProductStorage {
    private final Map<String, Integer> storage = new TreeMap<>();

    public void addProduct(String name, Integer quantity) {
        storage.put(name, quantity);
    }

    public void updateQuantity(String name) {
        storage.replace(name, storage.get(name)-1);
    }

    public void printProductStorage() {
        storage.forEach((p, q) ->
                System.out.println(Colors.ANSI_BRIGHT_WHITE + p + ", quantity: " + q + Colors.ANSI_RESET));
    }

    public int getProductFromStorage(String name) {
        return storage.get(name);
    }

}
