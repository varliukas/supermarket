import CashRegister.CashRegister;
import Colors.Colors;
import Product.Product;
import Product.ProductStorage;
import Supermarket.SupermarketServiceImpl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Supermarket {
    private static final List<Product> products = new ArrayList<>();
    private static final ProductStorage productStorage = new ProductStorage();
    private static final List<CashRegister> cashRegister = new ArrayList<>();
    private static SupermarketServiceImpl supermarketServiceImpl;

    public static void main(String[] args) {

        Supermarket supermarket = new Supermarket(
                "src\\main\\resources\\initialProducts.csv",
                "src\\main\\resources\\initialCashRegister.csv");

        printStoreAndCash("Initial");
        supermarketServiceImpl.runSupermarket(products, productStorage, cashRegister);
        printStoreAndCash("Updated");
    }

    private Supermarket(String initialProductsFile, String initialCashRegisterFile) {
        ReadCsvFiles.readProducts(initialProductsFile, products, productStorage);
        ReadCsvFiles.readCashRegister(initialCashRegisterFile, cashRegister);
        cashRegister.sort(Comparator.comparing(CashRegister::getValue));
        supermarketServiceImpl = SupermarketServiceImpl.getSupermarketServiceImpl();
    }

    private static void printStoreAndCash(String initialOrUpdated) {
        System.out.println(Colors.ANSI_BRIGHT_GREEN + "--------------------------------------------------" + Colors.ANSI_RESET);
        System.out.println(Colors.ANSI_BRIGHT_GREEN + initialOrUpdated + " Product Inventory: " + Colors.ANSI_RESET);
        productStorage.printProductStorage();
        System.out.println(Colors.ANSI_BRIGHT_GREEN + initialOrUpdated + " Cash Register: " + Colors.ANSI_RESET);
        cashRegister.forEach((c) -> System.out.println(Colors.ANSI_BRIGHT_WHITE +
                "Value: " + c.getValue() + ", quantity: " + c.getQuantity() + Colors.ANSI_RESET));
        System.out.println(Colors.ANSI_BRIGHT_GREEN + "--------------------------------------------------" + Colors.ANSI_RESET);
        System.out.println();
    }

}
